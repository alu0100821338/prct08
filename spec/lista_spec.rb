require 'spec_helper'
require 'lista'

describe Lista do
      LIBRO = Libro::Libro.new()
      aut01=%w{Dave.Thomas Andy.Hunt Chad.Fowler}
      Tit01 = "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide"
      Ser01 = "(The Facets of Ruby)"
      Edt01 = "Pragmatic Bookshelf"
      Edc01 = "4 edition"
      fch01 = "(July 7, 2013)"
      num01 = %w{ ISBN-13:978-1937785499 ISBN-10:1937785491 }
      
      LIBRO.setA(aut01)
      LIBRO.setT(Tit01)
      LIBRO.setS(Ser01)
      LIBRO.setEdt(Edt01)
      LIBRO.setEdc(Edc01)
      LIBRO.setFecha(fch01)
      LIBRO.setNum(num01)
  
  describe Nodo do
  it 'Existe nodo en la lista' do
        L1 = Lista::Lista.new()
        L1.insert(LIBRO)
        expect(L1.nodo_ini).to_not be_nil
  end
end

  describe Lista do
  it 'Se puede insertar un elemento en la lista' do
        L2 = Lista::Lista.new()
        L2.insert(LIBRO)
        expect(L2.nodo_ini).to_not be_nil
      end
      
  it 'Se pueden insertar varios elementos en la lista' do
        L3 = Lista::Lista.new()
        L3.insert(LIBRO)
        L3.insert(LIBRO)
        expect(L3.nodo_ini).to_not be_nil
        expect(L3.nodo_act).to_not be_nil
  end
      
  it 'Debe existir una Lista con su cabeza' do
       L4 = Lista::Lista.new()
       L4.insert(LIBRO)
       expect(L4.cabeza).to_not be_nil
       expect(L4.cabeza).to eq(L4.nodo_act)
  end
  
  it 'Se extrae el primer elemento de la lista y se muestra' do
       L5 = Lista::Lista.new()
       L5.insert(LIBRO)
       L5.insert(LIBRO)
       aux=L5.nodo_ini
     #  L5.mostrar()
       L5.extraer()
     #  L5.mostrar()
       expect(L5.nodo_ini).to eq(aux[1])
  end 
end

    describe "#LISTA DOBLEMENTE ENLAZADA" do
        it 'Existe LISTA doblemente enlazada' do
            L6 = Lista::Lista.new()
            L6.insert(1)
            L6.insert(2)
            L6.insert(3)
    
            expect(L6.nodo_ini).to_not be_nil
            
        end
        
        it 'Se inserta LISTA doblemente enlazada' do
            expect(L6.nodo_act[2].v).to eq(2) #el valor del anterior es 2
            aux=L6.nodo_act[2] 
            expect(aux[1].v).to eq(3) #el valor del siguiente del anterior es tres
        end
        
        it 'Se extrae LISTA doblemente enlazada' do
            L6.extraer()
            expect(L6.nodo_ini.v).to eq(2) 
            expect(L6.nodo_ini.b).to be_nil
        end
    end
    
    describe "#JERARQUÍA clase libro" do
    
            r1=Libro::Revista.new("HOLA")
            r1.setT("Título 1")
            
            p1=Libro::Periodico.new("Col. 3")
            p1.setT("Periodico 1")
            
            d1=Libro::Documento.new("http://rubyLibroLearn.io")
            d1.setT("Documento 1")
         
        it 'Creamos los objetos de clases hijas' do
            
            r1.formatea()
            p1.formatea()
            d1.formatea()
            
            expect(r1).to be_an_instance_of(Libro::Revista)
            expect(p1).to be_an_instance_of(Libro::Periodico)
            expect(d1).to be_an_instance_of(Libro::Documento)
             expect(r1).to be_an_instance_of(Object::Libro::Revista)

            
        end
        
        it 'Comprobamos que el padre es Libro' do
            expect(r1).to be_a_kind_of(Libro::Libro)
            expect(p1).to be_a_kind_of(Libro::Libro)
            expect(d1).to be_a_kind_of(Libro::Libro)
        end
        
        it 'Comprobamos de nuevo hijos Libro' do
            expect(r1.nombrerevista).to eq("HOLA")
            expect(p1.articulo).to eq("Col. 3")
            expect(d1.url).to eq("http://rubyLibroLearn.io")
        
        end
     it 'Comprobamos responde a un metodo' do
         expect(r1.respond_to?:nombrerevista).to eq(true)
     end
    end
    
end



